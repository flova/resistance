use axum::{
    extract::{
        ws::{Message, WebSocket, WebSocketUpgrade},
        Path,
    },
    http::{HeaderValue, Method, StatusCode},
    response::{IntoResponse, Json},
    routing::{get, post},
    Extension, Router,
};
use futures_util::sink::SinkExt;
use futures_util::stream::{SplitSink, StreamExt};
use once_cell::sync::OnceCell;
use rand::{distributions::Alphanumeric, seq::SliceRandom, Rng};
use serde_derive::{Deserialize, Serialize};
use serde_json::{json, Value};
use std::collections::{BTreeMap, HashSet};
use std::error::Error;
use std::net::SocketAddr;
use std::sync::Arc;
use tokio::sync::{mpsc, RwLock};
use tokio::time::{self, Duration};
use tower_cookies::{Cookie, CookieManagerLayer, Cookies, Key};
use tower_http::cors::CorsLayer;

async fn hello_world() -> &'static str {
    "Hello World!"
}

#[derive(Clone, Debug, PartialOrd, Ord, PartialEq, Eq, Serialize, Deserialize, Hash)]
struct UserId(String);

#[derive(Clone, Debug, PartialOrd, Ord, PartialEq, Eq)]
struct LobbyId(String);

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
enum Role {
    Spy,
    Resistance,
}

#[derive(Copy, Clone, Debug)]
struct RoundMeta<T>(T, T, T, T, T);

#[derive(Copy, Clone, Debug)]
enum RoundT<P, F> {
    Round0(F, F, F, F, F),
    Round1(P, F, F, F, F),
    Round2(P, P, F, F, F),
    Round3(P, P, P, F, F),
    Round4(P, P, P, P, F),
    Round5(P, P, P, P, P),
}

impl<P, F> Default for RoundT<P, F>
where
    F: Default,
{
    fn default() -> Self {
        RoundT::Round0(
            F::default(),
            F::default(),
            F::default(),
            F::default(),
            F::default(),
        )
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum GamePhase {
    Lobby,
    Choosing,
    Voting,
    Mission,
}

impl Default for GamePhase {
    fn default() -> Self {
        Self::Lobby
    }
}

#[derive(Clone, Debug, Default)]
struct GameState {
    roles: BTreeMap<UserId, Role>,
    round_results: RoundT<bool, ()>,
    num_votes: u8,
    participants: HashSet<UserId>,
    votes: BTreeMap<UserId, Option<bool>>,
    mission_votes: BTreeMap<UserId, Option<bool>>,
    leader: Option<UserId>,
    phase: GamePhase,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
#[serde(tag = "type", content = "value")]
enum GameMsgIn {
    SetName(String),
    ChooseTeam(HashSet<UserId>),
    SetVote(Option<bool>),
    SetMissionVote(Option<bool>),
    SetLeader(Option<UserId>),
    StartGame,
    VoteNow,
    MissionNow,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
#[serde(tag = "type")]
enum GameMsgOut {
    VoteStatus {
        votes: BTreeMap<UserId, Option<bool>>,
    },
    VoteResult {
        votes: BTreeMap<UserId, Option<bool>>,
        result: bool,
    },
    MissionResult {
        votes: BTreeMap<UserId, Option<bool>>,
        result: bool,
    },
    SelectedTeam {
        team: HashSet<UserId>,
    },
    ConnectedUsers {
        users: BTreeMap<UserId, String>,
    },
    YourId {
        id: UserId,
    },
    StartGame {
        known_roles: BTreeMap<UserId, Role>,
        leader: UserId,
        players: HashSet<UserId>,
    },
}

#[derive(Debug, Default)]
struct GameLobby {
    names: BTreeMap<UserId, String>,
    game: GameState,
    out_sinks: BTreeMap<UserId, SplitSink<WebSocket, Message>>,
}

impl GameLobby {
    async fn send_all_clients(&mut self, msg: &GameMsgOut) {
        let out_sinks = &mut self.out_sinks;
        let text = serde_json::to_string(msg).unwrap();
        for (_, sink) in out_sinks.iter_mut() {
            sink.send(Message::Text(text.clone())).await.unwrap();
        }
    }

    async fn start_game(&mut self) -> Result<(), Box<dyn Error>> {
        if self.game.participants.len() != 5 {
            return Ok(());
        }
        let (participants, leader, spies, agents) = {
            let mut rng = rand::thread_rng();
            let mut participants: Vec<UserId> = self.game.participants.iter().cloned().collect();
            participants.shuffle(&mut rng);
            let leader: UserId = (*participants.choose(&mut rng).unwrap()).clone();
            let (spies, agents) = participants.split_at(2);
            let owned_spies = spies.to_owned();
            let owned_agents = agents.to_owned();
            (participants, leader, owned_spies, owned_agents)
        };
        let spies_iter = spies.iter().cloned().map(|x| (x, Role::Spy));
        let agents_iter = agents.iter().cloned().map(|x| (x, Role::Resistance));
        self.game.roles = spies_iter.chain(agents_iter).collect();
        self.game.phase = GamePhase::Choosing;

        for agent in &agents {
            let sink = self.out_sinks.get_mut(agent).ok_or("no outsink")?;
            let mut known_roles = BTreeMap::new();
            known_roles.insert(agent.clone(), Role::Resistance);
            let msg = GameMsgOut::StartGame {
                known_roles,
                leader: leader.clone(),
                players: participants.iter().cloned().collect(),
            };
            let msg_text = serde_json::to_string(&msg).unwrap();
            sink.send(Message::Text(msg_text)).await?;
        }

        for spy in &spies {
            let sink = self.out_sinks.get_mut(spy).ok_or("no outsink")?;
            let known_roles: BTreeMap<UserId, Role> =
                spies.iter().map(|x| (x.clone(), Role::Spy)).collect();
            let msg = GameMsgOut::StartGame {
                known_roles,
                leader: leader.clone(),
                players: participants.iter().cloned().collect(),
            };
            let msg_text = serde_json::to_string(&msg).unwrap();
            sink.send(Message::Text(msg_text)).await?;
        }

        Ok(())
    }

    async fn send_connected_users(&mut self) {
        match self.game.phase {
            GamePhase::Lobby => {
                let users: BTreeMap<UserId, String> = self
                    .out_sinks
                    .keys()
                    .filter_map(|id| self.names.get(id).map(|name| (id.clone(), name.clone())))
                    .collect();
                self.send_all_clients(&GameMsgOut::ConnectedUsers { users })
                    .await;
            }
            _ => {
                let users = self
                    .game
                    .roles
                    .keys()
                    .map(|id| {
                        (
                            id.clone(),
                            self.names
                                .get(id)
                                .unwrap_or(&format!("Agent {}", id.0))
                                .clone(),
                        )
                    })
                    .collect();
                self.send_all_clients(&GameMsgOut::ConnectedUsers { users })
                    .await;
            }
        }
    }
}

#[derive(Debug)]
enum LobbyMessage {
    Game {
        user: UserId,
        message: GameMsgIn,
    },
    Connected {
        user: UserId,
        sink: SplitSink<WebSocket, Message>,
    },
    Disconnected {
        user: UserId,
    },
}

type LobbyIn = mpsc::Sender<LobbyMessage>;
type LobbyOut = mpsc::Receiver<LobbyMessage>;

struct AppState {
    lobbies: RwLock<BTreeMap<LobbyId, LobbyIn>>,
}

async fn websocket(stream: WebSocket, lobby_id: LobbyId, user_id: UserId, game_channel: LobbyIn) {
    // By splitting we can send and receive at the same time.
    tracing::debug!(
        "Splitting websocket stream lobby {:?}, user {:?}",
        &lobby_id,
        &user_id
    );
    let (sender, mut receiver) = stream.split();

    tracing::debug!(
        "Insert websocket sink {:?} into lobby {:?}",
        &user_id,
        &lobby_id
    );

    game_channel
        .send(LobbyMessage::Connected {
            user: user_id.clone(),
            sink: sender,
        })
        .await
        .unwrap();

    tracing::debug!("start forwarding messages");
    while let Some(Ok(Message::Text(text))) = receiver.next().await {
        tracing::debug!("forward mesage: {}", &text);
        let msg: Result<GameMsgIn, _> = serde_json::from_str(&text);
        match msg {
            Ok(message) => {
                tracing::debug!("sending {:?}", &message);
                game_channel
                    .send(LobbyMessage::Game {
                        user: user_id.clone(),
                        message,
                    })
                    .await
                    .unwrap();
            }
            Err(e) => {
                tracing::debug!("could not deserialize: {} {:?}", &text, e)
            }
        }
    }

    tracing::debug!("channel closed, lobby {:?}, user {:?}", &lobby_id, &user_id);

    game_channel
        .send(LobbyMessage::Disconnected {
            user: user_id.clone(),
        })
        .await
        .unwrap();
}

async fn websocket_handler(
    ws: WebSocketUpgrade,
    Extension(state): Extension<Arc<AppState>>,
    Path(lobby_id): Path<String>,
    cookies: Cookies,
) -> Result<impl IntoResponse, StatusCode> {
    let key = KEY.get().unwrap();
    let private_cookies = cookies.private(key);

    let rstring: String = rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(7)
        .map(char::from)
        .collect();
    let new_user_id = UserId(rstring);

    tracing::debug!("private token cookie: {:?}", private_cookies.get("token"));
    let user_id = private_cookies
        .get("token")
        .and_then(|c| c.value().parse().ok().map(UserId))
        .unwrap_or(new_user_id);

    // private_cookies.add(Cookie::new("token", user_id.0.clone()));

    let lobby = {
        let lobbies = state.lobbies.read().await;
        match lobbies.get(&LobbyId(lobby_id.clone())) {
            Some(lobby) => lobby.clone(),
            None => {
                tracing::debug!("Lobby with id {:?} does not exist", &lobby_id);
                return Err(StatusCode::BAD_REQUEST);
            }
        }
    };

    tracing::debug!("upgrading websocket");
    Ok(ws.on_upgrade(move |socket| websocket(socket, LobbyId(lobby_id), user_id, lobby)))
}

async fn run_game(
    state: Arc<AppState>,
    lobby_id: LobbyId,
    mut lobby: GameLobby,
    mut receiver: LobbyOut,
) {
    tracing::debug!("Start handling lobby");
    let timeout = time::sleep(Duration::from_secs(2 * 60 * 60));
    tokio::pin!(timeout);
    let timer_fut = time::sleep(Duration::from_secs(2 * 60 * 60));
    tokio::pin!(timer_fut);

    loop {
        tokio::select! {
            token = receiver.recv() => {
                let msg = match token {
                    Some(msg) => msg,
                    None => {
                        tracing::debug!("no more senders, closing lobby");
                        return;
                    }
                };

                match msg {
                    LobbyMessage::Connected{user, mut sink } => {
                        let id = serde_json::to_string(&GameMsgOut::YourId{ id: user.clone() }).unwrap();
                        if let Err(err) =  sink.send(Message::Text(id)).await {
                            tracing::debug!("error sending to {:?}, {:?}", &user, &err);
                        };
                        let old = lobby.out_sinks.insert(user.clone(), sink);
                        if let Some(mut old_sender) = old {
                            old_sender.close().await.unwrap();
                        }
                        lobby.names.entry(user.clone()).or_insert_with(|| format!("Agent {}", rand::random::<u16>()));
                        lobby.send_connected_users().await;
                        lobby.send_all_clients(&GameMsgOut::SelectedTeam{ team: lobby.game.participants.clone()}).await;
                    },
                    LobbyMessage::Disconnected{ user } => {
                        if lobby.game.phase == GamePhase::Lobby {
                            lobby.game.participants.remove(&user);
                        }
                        let old = lobby.out_sinks.remove(&user);
                        if let Some(mut old_sender) = old {
                            old_sender.close().await.unwrap();
                        }
                        lobby.send_connected_users().await;
                        lobby.send_all_clients(&GameMsgOut::SelectedTeam{ team: lobby.game.participants.clone()}).await;
                    },
                    LobbyMessage::Game{ user, message } => {
                        tracing::debug!("recived {:?}", &message);
                        use GameMsgIn::*;
                        match message {
                            SetName(name) => {
                                lobby.names.insert(user.clone(), name);
                                lobby.send_all_clients(&GameMsgOut::ConnectedUsers{ users: lobby.names.clone()}).await;
                            }
                            ChooseTeam(team) => {
                                lobby.game.participants = team;
                                lobby.send_all_clients(&GameMsgOut::SelectedTeam { team: lobby.game.participants.clone() }).await;
                            }
                            SetVote(vote) => {
                                lobby.game.votes.insert(user.clone(), vote);
                            }
                            SetMissionVote(vote) => {
                                lobby
                                    .game
                                    .mission_votes
                                    .insert(user.clone(), vote);
                            }
                            SetLeader(leader) => {
                                lobby.game.leader = leader;
                            },
                            StartGame => {
                                lobby.start_game().await.unwrap();
                            }
                            VoteNow => {
                                timer_fut.set(time::sleep(Duration::from_secs(5)));
                            }
                            MissionNow => {
                                timer_fut.set(time::sleep(Duration::from_secs(5)));
                            }
                        }
                    }
                };
            },
            _ = &mut timeout => {
                tracing::debug!("lobby timed out");
                break;
            }
            _ = &mut timer_fut => {
                tracing::debug!("reached timeout!");
                timer_fut.set(time::sleep(Duration::from_secs(2 * 60 * 60)));
                match lobby.game.phase {
                    GamePhase::Voting => {
                        let votes = lobby.game.votes.clone();
                        let pro_votes = lobby.game.votes.values().filter(|&&vote| vote == Some(true)).count();
                        let success = pro_votes > lobby.game.roles.len() / 2;
                        lobby.send_all_clients(&GameMsgOut::VoteResult{ votes, result: success }).await;
                        if success {
                            lobby.game.phase = GamePhase::Mission;
                        } else {
                            lobby.game.num_votes += 1;
                        }
                    },
                    GamePhase::Mission => {
                        let mission_votes = lobby.game.mission_votes.clone();
                        let pro_votes = lobby.game.mission_votes.values().filter(|&&vote| vote == Some(true)).count();
                        let success = pro_votes >= lobby.game.participants.len();
                        lobby.send_all_clients(&GameMsgOut::MissionResult { votes: mission_votes, result: success }).await;
                        lobby.game.phase = GamePhase::Voting;
                        lobby.game.num_votes = 0;
                        lobby.game.participants = HashSet::default();
                    }
                    _ => {

                    },
                }
            },
            else => break,
        }
    }

    tracing::debug!("game over, removing lobby");
    {
        let mut lobbies = state.lobbies.write().await;
        lobbies.remove(&lobby_id);
    }
}

async fn create_lobby(
    Extension(state): Extension<Arc<AppState>>,
) -> Result<Json<Value>, StatusCode> {
    let (sender, reciever) = mpsc::channel(20);
    let rstring: String = rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(7)
        .map(char::from)
        .collect();
    let lobby_id = LobbyId(rstring);
    let lobby = GameLobby::default();
    {
        let mut lobbies = state.lobbies.write().await;
        if lobbies.contains_key(&lobby_id) {
            tracing::debug!("lobby with id {:?} already exists", lobby_id);
            return Err(StatusCode::INTERNAL_SERVER_ERROR);
        }
        lobbies.insert(lobby_id.clone(), sender);
        tracing::debug!("lobby count {}", &lobbies.len());
    }
    tokio::spawn(run_game(state.clone(), lobby_id.clone(), lobby, reciever));
    Ok(Json(json!({"lobby_id": lobby_id.0.clone() })))
}

static KEY: OnceCell<Key> = OnceCell::new();

#[tokio::main]
async fn main() {
    // initialize tracing
    tracing_subscriber::fmt::init();

    let my_key: &[u8] = &[0; 64]; // Your real key must be cryptographically random
    KEY.set(Key::from(my_key)).ok();

    let state = Arc::new(AppState {
        lobbies: RwLock::new(BTreeMap::default()),
    });

    let app = Router::new()
        .route("/", get(hello_world))
        .route("/create_lobby", post(create_lobby))
        .layer(
            CorsLayer::new()
                .allow_origin("http://localhost:3000".parse::<HeaderValue>().unwrap())
                .allow_methods([Method::GET]),
        )
        .layer(Extension(state.clone()))
        .route("/ws/:lobby_id", get(websocket_handler))
        .layer(Extension(state.clone()))
        .layer(CookieManagerLayer::new());

    // run our app with hyper
    // `axum::Server` is a re-export of `hyper::Server`
    let addr = SocketAddr::from(([127, 0, 0, 1], 3001));
    tracing::debug!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}
