import { useState, useEffect, createContext, useContext } from 'react';

import AvatarSvg from './avatar.svg';
import LeaderSvg from './leader.svg';
import ProSvg from './checkmark.svg';
import ContraSvg from './checkcross.svg';

import './App.css';

import { BrowserRouter, Routes, Route, useNavigate, useParams } from "react-router-dom";

const GameContext = createContext({
  players: [],
  others: [],
  me: {},
  votes: {},
  handleNameChange() { },
  handleParticipantChange() { },
  handleStartGame() { },
  handleVoteChange() { },
  handleVoteNow() { },
});

function Mission({ num, participants }) {
  return <div className="mission circle">
    <span style={{ whiteSpace: 'nowrap' }}>Mission {num}</span>
    <span className="mission-number">{participants}</span>
  </div>;
}

function Missions() {
  return <div className="missions">
    {[2, 3, 2, 3, 3].map((v, i) => <Mission key={i} num={i + 1} participants={v} />)}
  </div>;
}

function Vote({ num }) {
  return <div className="vote">
    <div className='voteContent'>
      {num}
    </div>
  </div>;
}

function Votes() {
  return <div className="votes">
    {[1, 2, 3, 4, 5].map((v, i) => <Vote num={v} />)}
  </div>;
}

function Board() {
  return (
    <div className="board board-box">
      <Missions />
      <Votes />
      <button>Start Voting Now!</button>
    </div>
  );
}

function Avatar({ name, leader, participant, role, toggleSelect }) {
  let avatarClasses = ['avatar', 'card-background'];
  if (['spy', 'resistance'].includes(role)) {
    avatarClasses.push(role);
  }
  if (participant) {
    avatarClasses.push('participant');
  }
  return <div className={avatarClasses.join(' ')} onClick={() => toggleSelect && toggleSelect()}>
    <img className="avatar-img" alt={name} src={AvatarSvg}></img>
    <div className='avatar-name'>{name}</div>
    <div className="leaderPlaceholder">
      {leader && <img alt="Leader" src={LeaderSvg}></img>}
    </div>
  </div>;
}

function VoteCards() {
  return <div className='vote-cards'>
    <div className='vote-card' />
    <div className='vote-card' />
  </div>
}

function OtherPlayer({ player }) {
  const { handleParticipantChange } = useContext(GameContext);
  return <div className="other-player">
    <VoteCards />
    <Avatar
      name={player.name}
      leader={player.leader}
      role={player.role}
      participant={player.participant}
      toggleSelect={() => handleParticipantChange(player.id)}
    />
  </div>;
}

function OtherPlayers() {
  const { others } = useContext(GameContext);
  return <div className="other-players">
    {others.map(player => <OtherPlayer
      key={player.id}
      player={player}
    />)}
  </div>;
}

function OwnSelection() {
  return <div className="vote-selection">
    <div className="ownvote pro">
      <img alt="pro" src={ProSvg}></img>
    </div>
    <div className="ownvote contra">
      <img alt="contra" src={ContraSvg}></img>
    </div>
  </div>;
}

function OwnPlayer() {
  const { me, handleParticipantChange } = useContext(GameContext);
  return <div className="own-player">
    <Avatar
      name={me.name}
      leader={me.leader}
      participant={me.participant}
      role={me.role}
      toggleSelect={() => handleParticipantChange(me.id)}
    />
    <OwnSelection />
  </div>;
}

function Field() {

  return <div className="field">
    <OtherPlayers />
    <Board />
    <OwnPlayer />
  </div>
}

function Index() {
  const navigate = useNavigate();
  const createLobby = async () => {
    console.log('creating lobby...');
    const opts = { method: 'POST', body: '' };
    const res = await fetch('http://localhost:3001/create_lobby', opts).then(response => response.json());
    console.log(`created lobby: ${res.lobby_id}`);
    console.log(res);
    navigate(`/lobby/${res.lobby_id}`)
  };
  return <div>
    <h1>The Resistance</h1>
    <button onClick={createLobby}>Create Lobby</button>
  </div>;
}

function playerFromState(state, player_id) {
  let player = { id: player_id };
  player.name = state.names[player_id];
  player.role = state.roles[player_id];
  player.leader = player_id === state.leader;
  player.participant = state.participants.includes(player_id);
  if (state.votes.hasOwnProperty(player_id)) {
    player.vote = state.votes[player_id];
  }
  return player;
}

function Lobby() {
  const { players, me, num_participants, handleNameChange, handleParticipantChange, handleStartGame } = useContext(GameContext);
  return <div style={{ display: 'flex', flexDirection: 'column', gap: 16, justifyContent: 'center', alignItems: 'center' }}>
    <div className="lobbyPlayers">
      {players.map(player => <Avatar
        key={player.id}
        name={player.name}
        leader={player.leader}
        participant={player.participant}
        toggleSelect={() => handleParticipantChange(player.id)}
      />)}
    </div>
    <div className="mission-form board-box">
      <form action="#" onSubmit={(e) => { e.preventDefault(); handleStartGame(); }}>
        <div className="form-pair">
          <label>Number <span style={{ whiteSpace: 'nowrap'}}>of Players</span></label>
          <input type="number" min="5" max="10" defaultValue={5} disabled></input>
        </div>
        <div className="form-pair">
          <span>Members <span style={{ whiteSpace: 'nowrap'}}>per Mission</span></span>
          <div className="mission-configs">
            <div className="mission-config">
              <label>Mission 1</label>
              <input type="number" min="1" max="10" defaultValue={2} disabled></input>
            </div>
            <div className="mission-config">
              <label>Mission 2</label>
              <input type="number" min="1" max="10" defaultValue={3} disabled></input>
            </div>
            <div className="mission-config">
              <label>Mission 3</label>
              <input type="number" min="1" max="10" defaultValue={2} disabled></input>
            </div>
            <div className="mission-config">
              <label>Mission 4</label>
              <input type="number" min="1" max="10" defaultValue={3} disabled></input>
            </div>
            <div className="mission-config">
              <label>Mission 5</label>
              <input type="number" min="1" max="10" defaultValue={3} disabled></input>
            </div>
          </div>
        </div>
        <div className="form-pair">
          <label>Choose Name</label>
          <input type="text" value={me.name ?? ""} onChange={e => handleNameChange(e.target.value)}></input>
        </div>
        <div className="buttons">
          <div className="copy-link">
            <button>Copy Link</button>
          </div>
          <div className="start-game">
            <button type="submit" disabled={num_participants !== 5}>Start Game! {num_participants}/5</button>
          </div>
        </div>
      </form>
    </div>
  </div>;
}

function Management() {
  const { lobby_id } = useParams();
  const [state, setState] = useState({
    players: [],
    names: {},
    roles: {},
    participants: [],
    my_id: undefined,
    leader: undefined,
    votes: {},
    status: 'lobby',
  });
  const [ws, setWs] = useState(undefined);
  useEffect(() => {
    console.log("opening websocket", lobby_id);
    const thisws = new WebSocket(`ws://localhost:3001/ws/${lobby_id}`);
    thisws.addEventListener('message', event => {
      const payload = JSON.parse(event.data);
      if (payload.type === 'connected_users') {
        setState(s => ({ ...s, names: payload.users }));
      }
      if (payload.type === 'your_id') {
        setState(s => ({ ...s, my_id: payload.id }));
      }
      if (payload.type === 'selected_team') {
        console.log(payload);
        setState(s => ({ ...s, participants: payload.team }));
      }
      if (payload.type === 'start_game') {
        setState(s => ({
          ...s,
          roles: payload.known_roles,
          players: payload.players,
          leader: payload.leader,
          status: 'playing',
          participants: [],
        }));
        console.log(payload);
      }
    });
    setWs(thisws);
    return () => { console.log("closing websocket"); thisws.close(); };
  }, [lobby_id]);

  const handleNameChange = name => {
    ws.send(JSON.stringify({ type: 'set_name', value: name }));
  }

  const handleParticipantChange = participant => {
    let participants = [...state.participants];
    if (participants.includes(participant)) {
      participants.splice(participants.indexOf(participant), 1);
    } else {
      participants.push(participant);
    }
    ws.send(JSON.stringify({ type: 'choose_team', value: [...participants] }));
  }

  const handleStartGame = () => ws.send(JSON.stringify({ type: 'start_game' }));

  const handleVoteNow = () => { };
  const handleVoteChange = (vote) => { ws.send(JSON.stringify({ type: 'set_vote', value: vote })) };

  const players = Object.keys(state.names).map(id => playerFromState(state, id));
  let me = playerFromState(state, state.my_id);
  
  return <div>
    <GameContext.Provider value={{
      players,
      others: players.filter(player => player.id !== me.id),
      me,
      participants: state.participants,
      votes: state.votes,
      leader: state.leader,
      num_participants: Object.keys(state.participants).length,
      handleNameChange,
      handleParticipantChange,
      handleStartGame,
      handleVoteChange,
      handleVoteNow,
    }}>
      {state.status === 'lobby' && <Lobby />}
      {state.status !== 'lobby' && <Field />}
    </GameContext.Provider>
  </div>;
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <BrowserRouter>
          <Routes>
            <Route index element={<Index />}></Route>
            <Route path="lobby/:lobby_id" element={<Management />}></Route>
            <Route path="game" element={<Field />}></Route>
          </Routes>
        </BrowserRouter>
      </header>
    </div>
  );
}

export default App;
